# -*- coding: utf-8 -*-
"""
Created on Sat Jan  4 20:47:01 2020

@author: pranj
"""

import datetime 
import hashlib
import json
from flask import Flask, jsonify

#Build a blockchain

class Blockchain:
    
    def __init__(self):
        self.chain = []
        self.create_block(proof = 1, previous_hash = '0')
        
    def create_block(self, proof, previous_hash):
        block = {'index': len(self.chain) + 1,
                 'timestamp': str(datetime.datetime.now()),
                 'proof': proof,
                 'previous_hash': previous_hash}
        
        self.chain.append(block)
        return block
    
    #Utility function to get the last block.
    def get_previous_block(self):
        return self.chain[-1]
    
    def get_proof_of_work(self, previous_proof):
        new_proof = 1
        check_proof = False
        while check_proof is False:
            hash_operation = hashlib.sha256(str(new_proof**2 - previous_proof**2).encode()).hexdigest() #The problem to solve
            if hash_operation[:4] == '0000': #The constraint to be solved. The hash should start with 0000
                check_proof = True
            else:
                new_proof += 1
        return new_proof
    
    def hash(self, block):
        encoded_block = json.dumps(block, sort_keys = True).encode()
        return hashlib.sha256(encoded_block).hexdigest()
    
    def is_chain_valid(self, chain):
        previous_block = chain[0]
        block_index = 1;
        while block_index < len(chain):
            current_block = chain[block_index]
            if current_block['previous_hash'] != self.hash(previous_block):
                return False
            previous_proof = previous_block['proof']
            current_proof = current_block['proof']
            hash_operation = hashlib.sha256(str(current_proof**2 - previous_proof**2).encode()).hexdigest()
            if hash_operation[:4] != '0000':
                return False
            previous_block = current_block
            block_index += 1
        return True

#Mining the above blockchain

# 1. Creating a web app.to interact with the blockchain
app = Flask(__name__)

#2. Creating the blockchain
blockchain = Blockchain()

@app.route('/mineBlock', methods=['GET'])
def mine_block():
    previous_block = blockchain.get_previous_block()
    previous_proof = previous_block['proof']
    proof = blockchain.get_proof_of_work(previous_proof) #Solve the problem and get the proof of work. No need to solve here.
    previous_hash = blockchain.hash(previous_block)
    block = blockchain.create_block(proof, previous_hash)
    response = {'message': 'Congratulations, you just mined a block!',
                'index': block['index'],
                'timestamp': block['timestamp'],
                'proof': block['proof'],
                'previous_hash': block['previous_hash']}
    return jsonify(response), 200

@app.route('/getChain', methods = ['GET'])
def get_chain():
    response = {'chain': blockchain.chain,
                'chain_length': len(blockchain.chain)}
    return jsonify(response), 200 

@app.route('/isChainValid', methods=['GET'])
def get_chain_status():
    blockchain_status = blockchain.is_chain_valid(blockchain.chain)
    response = {'status': blockchain_status}
    return jsonify(response), 200

app.run(host = '0.0.0.0', port = 5000)